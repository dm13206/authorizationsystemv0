<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html> 
<html> 
<head>
	<meta charset='utf-8'>
	<title>Log In</title>
	<link rel='stylesheet' href='gray.css'>
	
</head>
<body>
 <section id="container">
  <section id="content">
   <section id="whitebox">
     <table>
	<tr>
	 <td><h1>Hello Anonymous!</h1></td>
	</tr>
	<tr><td>Please log in to continue</td></tr>
	<tr>
	 <td>Username:</td>
	 <td><input type="text" name="login" value="login"></td>
	</tr>
	<tr>
	 <td>Password:</td>
	 <td><input type="text" name ="passwd" value="passwd"></td>
	</tr>
	<tr>
	 <td>&nbsp;</td>
	 <td><input type="submit" value="Log in" ></td>
	</tr>
	<tr>
	 <td>Don't have an account? <a href="newUser.jsp">Sign up!</a></td>
	</tr>

	
     </table>
   </section>
  </section>
 </section>
</body>
</html>